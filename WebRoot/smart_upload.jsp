<%@page import="java.io.File"%>
<%@page import="org.gpf.util.IPTimeStamp"%>
<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ page import="org.lxh.smart.*"%>
<%
	SmartUpload smart = new SmartUpload();
	smart.initialize(pageContext);			
	smart.upload();							
	IPTimeStamp ipts = new IPTimeStamp(request.getRemoteAddr()); 	// 取得客户端ip
	
	for(int i = 0;i<smart.getFiles().getCount();i++){
		String ext = smart.getFiles().getFile(i).getFileExt(); 			//　得到文件拓展名
		String filename = ipts.getIPTimeRand() + "." + ext;
		smart.getFiles().getFile(i).saveAs(this.getServletContext().getRealPath("/") + "smart_upload" + File.separator + filename);
%>
	

<!-- 显示刚刚上传的图片 -->
<%	
	// 利用正则匹配
		if(smart.getFiles().getFile(i).getFileName().matches("^\\w+.(jpg|gif|png)$")){
			pageContext.setAttribute("filename", filename);
%>
			<img src="smart_upload/${filename }" />
<%	
		}
	}
 %>
