<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%--
	List info = (List)request.getAttribute("info");
	if(info != null) {
		Iterator<String> iterator = info.iterator();
		while (iterator.hasNext()) {
			pageContext.setAttribute("info", iterator.next());
--%>
		<!-- <h4><font color="red">${info }</font></h4> -->
<%--		
		}
	}
 --%>
 
 <!-- 使用JSTL替代scriptlet代码进行迭代输出 -->
 <%pageContext.setAttribute("info", (List)request.getAttribute("info")); %>
 <%@taglib prefix="c" uri="http://127.0.0.1/JavaUserAdmin/jstl/core" %>
 <c:forEach items="${info }" var="info"><h4><font color="red">${info }</font></h4></c:forEach>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>用户登录</title>
  	<script type="text/javascript">
  		
  		var isFormSubmit = false;	//	表单是否提交
  		var xhr;
  		
  		/* 创建XHR */
  		function createXHR(){
  		
  			if(window.XMLHttpRequest){
				xhr = new XMLHttpRequest(); // FF,Chrome
			}else{
				xhr = new ActiveXObject();	// IE
			}
  		}
  	
  		function checkImage(checkcode){
  			
  			createXHR();
  			xhr.open("get", "ValidateImageServlet?checkcode=" + checkcode);
  			xhr.onreadystatechange = checkImageCallback;
  			xhr.send(null);
  			document.getElementById('msg').innerHTML = '正在验证。。。';
  		}
  	
  		function checkImageCallback(){
  		
  			if(xhr.readyState == 4 && xhr.status == 200){
				var text = xhr.responseText;
				if(text == 'validate'){
					document.getElementById('msg').innerHTML = '验证码正确！';
					isFormSubmit = true;
				}else{
					alert('验证码错误！');
					isFormSubmit = false;
				}
			}
  		}
  	
  		/* 客户端验证脚本，如果浏览器验证不通过就没有必要发送到Servlet进行验证了！可以降低服务器的负担 */
  		function checkUsernameAndPass(f){
  			if(!(/^\w{5,15}$/.test(f.username.value))){ // javascript的正则格式  /^匹配模式$/.test(待匹配的字符串)
  				alert("用户名必须是5~15位");
  				f.username.select();	// 将制定的表单内容选中，提升用户体验
  				isFormSubmit  = false;
  			}
  			if(!(/^\w{5,15}$/.test(f.password.value))){
  				alert("密码必须是5~15位");
  				f.password.select();
  				isFormSubmit  = false;
  			}
  		}
  		
  		function validate(f){
  			checkUsernameAndPass(f);
  			return isFormSubmit;
  		}
  		
  		/* 请求新的验证码图片 */
  		function reloadCode(){
  			// 给URL传递新的参数可以清空浏览器的缓存，让浏览器认为这是一个新的请求
  			var d = new Date();
  			document.getElementById('pic').src = '<%=request.getContextPath()%>/ImageServlet?d=' + d;
  		}
  		
  		
  	</script>
  </head>
  
  <body onload="loadSafeCode()">
	<form action="LoginServlet" method="post" onsubmit="return validate(this)"> 
		用户名:<input type="text" name="username"><br>
		密码:<input type="password" name="password"><br>
		验证码（不分大小写）：<input type="text" name="checkcode" size="4" onblur="checkImage(this.value)"><img alt="验证码" src="<%=request.getContextPath()%>/ImageServlet" id="pic" ><br />
		<a href="javascript:reloadCode();">看不清楚</a>&nbsp;&nbsp;<span id='msg'></span><br />
		<input type="submit" value="提交"><input type="reset" value="重置">
	</form>
  </body>
</html>
