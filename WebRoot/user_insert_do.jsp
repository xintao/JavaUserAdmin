<%@page import="org.gpf.factory.DAOFactory"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.gpf.vo.User"%>
<%@page contentType="text/html; charset=utf-8" %>
<%
	User user = new User();
	user.setUsername(request.getParameter("username"));
	user.setPassword(request.getParameter("password"));
	user.setAge(Integer.parseInt(request.getParameter("age")));
	user.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("birthday")));
	user.setSex(request.getParameter("sex"));
	
	if(DAOFactory.getIUserDAOInstance().doCreate(user)){
%>
	<h2>雇员信息添加成功！</h2>
<%	
	}else{
%>
	<h2>雇员信息添加失败！</h2>
<%	
	}
%>
