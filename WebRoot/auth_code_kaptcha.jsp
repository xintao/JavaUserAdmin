<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>使用开源jar包kaptcha实现验证码</title>
    <script type="text/javascript">
    	function changeImage(node){
    		node.src = 'kaptcha.jpg?time=' + new Date().getTime();
    	}
    </script>
  </head>
  
  <body>
    <form action="check_code_kaptcha.jsp" method="post">
     	<img src="kaptcha.jpg" onclick="changeImage(this)" style="cursor: pointer;"/> <input type="text" name="kaptcha" value="" />
     	<input type="submit"/>
	</form>
  </body>
</html>
