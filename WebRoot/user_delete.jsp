<%@page import="org.gpf.factory.DAOFactory"%>
<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>删除雇员</title>
  </head>
  
  <body> 
	<form action="user_delete.jsp">
		请输入要删除的雇员id:<input type="text" name="id">
		<input type="submit" value="提交">
	</form>
<%
	if(request.getParameter("id") != null) {
		int id = Integer.parseInt(request.getParameter("id"));
	
		if (DAOFactory.getIUserDAOInstance().doDelete(id)) {
%>
			<h2>雇员信息删除成功！</h2>
<%	
		}else{
%>
			<h2>雇员信息删除失败！</h2>
<%	
		}
	}
 %>		    
  </body>
</html>
