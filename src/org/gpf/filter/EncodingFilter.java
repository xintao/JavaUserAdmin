package org.gpf.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class EncodingFilter implements Filter {

	private String charset;
	
	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {

			if (charset == null)
				charset = "utf-8"; // 如果初始化参数中没有指定字符集，则使用utf-8
			req.setCharacterEncoding(charset);	// 每一个请求都会到达次过滤器，设置字符集
			chain.doFilter(req, resp);	// 放行
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		
		charset = config.getInitParameter("charset"); // 从初始化参数中读取字符集
	}

}
