package org.gpf.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 从标准输入中读取字符串、数字、日期
 * @author gaopengfei
 * @date 2015-4-23 下午8:47:20
 */
public class InputDate {

	private BufferedReader br = null;
	
	public InputDate() {
		this.br = new BufferedReader(new InputStreamReader(System.in));
	}
	
	public int getInt(String info,String error){
		
		int data = 0;
		boolean flag = true;
		while (flag) {
			String str = this.getString(info);
			if (str.matches("\\d+")) {
				data = Integer.parseInt(str);
				flag = false;
			}
			else
				System.out.println(error);
		}
		return data;
	}

	public String getString(String info) {
		
		String data = null;
		System.out.println(info);
		try {
			data = this.br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}
	
	public Date getDate(String info,String error){
		
		Date date = null;
		boolean flag = true;
		while (flag) {
			String str = this.getString(info);
			if (str.matches("\\d{4}-\\d{2}-\\d{2}")) {
				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(str);
					flag = false;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}else {
				System.out.println(error);
			}
		}
		return date;
	}
}
