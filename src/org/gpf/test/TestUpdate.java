package org.gpf.test;

import java.util.Date;

import org.gpf.factory.DAOFactory;
import org.gpf.vo.User;

public class TestUpdate {

	public static void main(String[] args) {

		User user = new User();
		user.setUsername("张三");
		user.setPassword("666666");
		user.setAge(23);
		user.setSex("女");
		user.setId(2);
		user.setBirthday(new Date());
		DAOFactory.getIUserDAOInstance().doUpdate(user);
	}

}
