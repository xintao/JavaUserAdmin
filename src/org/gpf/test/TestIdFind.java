package org.gpf.test;

import org.gpf.factory.DAOFactory;
import org.gpf.vo.User;

public class TestIdFind {

	public static void main(String[] args) {

		User user = DAOFactory.getIUserDAOInstance().findById(2);
		System.out.println(user);
	}

}
