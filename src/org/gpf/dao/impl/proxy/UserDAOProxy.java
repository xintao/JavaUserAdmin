package org.gpf.dao.impl.proxy;

import java.util.List;

import org.gpf.dao.IUserDAO;
import org.gpf.dao.impl.UserDAOImpl;
import org.gpf.dbc.DBConnection;
import org.gpf.vo.User;
/**
 * 代理操作类，可以对真实的操作类进行包装，并附加其他的操作（数据库的连接和释放）
 * @author gaopengfei
 * @date 2015-4-23 下午6:57:51
 */
public class UserDAOProxy implements IUserDAO{

	private DBConnection dbc = null;
	private IUserDAO dao = null;		
	
	public UserDAOProxy() {
		this.dbc = new DBConnection();
		this.dao = new UserDAOImpl(this.dbc.getConnect()); // 持有对真实主题的引用
	}
	
	@Override
	public boolean doCreate(User user) {
		boolean flag = this.dao.doCreate(user);
		this.dbc.close();
		return flag;
	}

	@Override
	public boolean doUpdate(User user) {
		boolean flag = this.dao.doUpdate(user);
		this.dbc.close();
		return flag;
	}

	@Override
	public boolean doDelete(int id) {
 		boolean flag = this.dao.doDelete(id);
 		this.dbc.close();
 		return flag;
	}

	@Override
	public User findById(int id) {
 		User user = null;
 		user = this.dao.findById(id);
 		this.dbc.close();
 		return user;
	}

	@Override
	public List<User> findAll(String keyword) {
		List<User> all = null;
 		all = this.dao.findAll(keyword);
 		this.dbc.close();
 		return all;
	}

	@Override
	public boolean findLogin(User user) {

		boolean flag = this.dao.findLogin(user);
		this.dbc.close();
		return flag;
	}

	@Override
	public boolean isUsernameExists(String username) {
		
		boolean flag = this.dao.isUsernameExists(username);
		this.dbc.close();
		return flag;
	}

}
