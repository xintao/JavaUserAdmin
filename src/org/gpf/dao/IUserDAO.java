package org.gpf.dao;

import java.util.List;

import org.gpf.vo.User;

/**
 * 数据访问接口
 */
public interface IUserDAO {

	public boolean doCreate(User user); 				// DB创建
	public boolean doUpdate(User user);					// DB更新
	public boolean doDelete(int id);					// DB删除
	public User findById(int id);						// 根据id查找
	public boolean findLogin(User user);				// 登录验证
	public List<User> findAll(String keyword);			// 查询多个
	public boolean isUsernameExists(String username);	// 用户名是否合法
}
