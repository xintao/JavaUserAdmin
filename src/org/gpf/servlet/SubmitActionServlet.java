package org.gpf.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.octo.captcha.module.servlet.image.SimpleImageCaptchaServlet;
/**
 * 图片验证码的captcha实现
 * @author gaopengfei
 * @date 2015-5-20 下午9:58:20
 */
public class SubmitActionServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String userCaptchaResponse = request.getParameter("japtcha");
		boolean captchaPassed = SimpleImageCaptchaServlet.validateResponse(request, userCaptchaResponse);
		
		response.setContentType("text/html;charset=utf-8");
		if (captchaPassed)
			response.getWriter().write("验证通过！");
		else {
			response.getWriter().write("验证失败！");
		}
		response.getWriter().write("<br/><a href='auth_code_jcaptcha.jsp'>重新验证</a>");
	}
}