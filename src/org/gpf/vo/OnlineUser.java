package org.gpf.vo;

import java.util.Date;
/**
 * 在线用户实体
 * @author gaopengfei
 * @date 2015-4-30 下午8:14:47
 */
public class OnlineUser {

	private String sessionID;
	private String username;
	private String ip;
	private Date firstAccessTime;
	
	public OnlineUser() {
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public void setFirstAccessTime(Date firstAccessTime) {
		this.firstAccessTime = firstAccessTime;
	}
	@Override
	public String toString() {
		return "sessionID=" + sessionID + ", username=" + username
				+ ", ip=" + ip + ", firstAccessTime=" + firstAccessTime;
	}
	
	
}
