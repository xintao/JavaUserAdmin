package org.gpf.menu;

import org.gpf.operation.Operation;
import org.gpf.util.InputDate;

public class Menu {

	public Menu() {
		while (true) {
			this.showMenu();
		}
	}

	/**
	 * 显示菜单，业务逻辑交给Operation类完成
	 */
	private void showMenu() {
		
		System.out.println("========== XXX人员管理系统 ==========");
		System.out.println("1.增加用户");
		System.out.println("2.删除用户");
		System.out.println("3.修改用户");
		System.out.println("4.查找单个用户");
		System.out.println("5.查找全部用户");
		System.out.println("6.退出系统");
		
		InputDate input = new InputDate();
		int ch = input.getInt("\n请选择:", "请输入正确的选项！");
		switch (ch) {
		case 1:
			System.out.println("你选择了增加！");
			Operation.insert();
			break;
		case 2:
			System.out.println("你选择了删除！");
			Operation.delete();
			break;
		case 3:
			System.out.println("你选择了修改！");
			Operation.update();
			break;
		case 4:
			System.out.println("你选择了查找单个用户！");
			Operation.findById();
			break;
		case 5:
			System.out.println("你选择了查询所有用户！");
			Operation.findAll();
			break;
		case 6:
			System.out.println("你选择了退出！");
			System.exit(0);
			break;
		default:
			System.out.println("请选择正确的选项！");
			break;
		}
	}
	
}
